<!DOCTYPE html>
<html>
  <head>
    <title>Pre registrer | Caasa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" type="text/css" />
    <link rel="stylesheet" href="css/style.min.css" type="text/css" />
    <!--link rel="stylesheet" href="css/firebaseui.css" type="text/css" /-->
    <link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/3.0.0/firebase-ui-auth.css" />    
    <!--script src="js/firebaseui.js"></script-->
    <script src="https://www.gstatic.com/firebasejs/ui/3.0.0/firebase-ui-auth__it.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
    <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.0.1/firebase-auth.js"></script>   
    <script src="https://www.gstatic.com/firebasejs/3.1.0/firebase-database.js"></script>    
    <script src="js/initFirebase.js"></script>
    <script src="js/loginController.js"></script>
    <script src="js/loginViewer.js"></script>
    <script src="js/firebaseAuthentication.js"></script>
    <!--script src="js/app.js"></script-->
  </head>
  <body>
      
      
        <section class="w-login">
            <img class="w-login__logo" src="images/main-logo.png">
            <!--form id="caasalogin" class="w-login__form clearfix" action="/ws/cuaa/login" method="post"-->
            <div id="caasalogin" class="w-login__form clearfix">
                <!-- User Signed Out -->
                <div id="user-signed-out" class="form-horizontal">
                    <h1 class="w-login__title">Effettua l'accesso</h1>
                    <div id="firebaseui-container"></div>
                    <div class="w-login__divider">
                        <strong class="divider-title">oppure</strong>
                    </div>
                    <form id="FormLogin" method="post" class="form-horizontal" action="">
                    <div class="form-group">
                        <label class="w-login__label" for="emailAuth">Email</label>
                        <div class="input-field">
                            <input name="userid" type="email" class="form-control" id="emailAuth" placeholder="Email" required>
                        </div>    
                    </div>                   
                    <div class="form-group">
                        <label class="w-login__label" for="password">Password</label>
                        <div class="input-field">
                            <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
                        </div>    
                    </div>                    
                    <button class="btn btn-default pull-right" id="quickstart-sign-in">Accedi</button>
                    <br />
                    <div class="checkbox">
                        <a href="#" class="custom-link custom-link--block-left" id="show-reset-password">Non ricordi la password?</a>
                    </div>                    
                    </form>
                </div>  
                <!-- End User Signed Out -->
                
                <!-- User Signed In -->
                  <div id="user-signed-in">
                    <h1 class="w-login__title"><div id="name" style="display: inline-block;"></div></h1>
                    <div id="user-info" >
                      <div id="photo-container">
                        <img id="photo">
                      </div>
                      <div id="email"></div>
                      
                      <!--div id="phone"></div-->
                      <!--div id="is-new-user"></div-->
                      <div class="clearfix"></div>
                    </div>
                    <br>
                    <p>
                      &nbsp;&nbsp;
                      <button class="btn btn-default pull-right" id="sign-out">Sign Out</button>
                      &nbsp;&nbsp;
                      <button class="btn btn-default pull-right" id="delete-account">Delete account</button>
                    </p>
                  </div>
                <!-- End User Signed In -->  
                
            <!--Registrati-->
            <div id="user-register">
                <h1 class="w-login__title">Registrati</h1>
                    <form id="FormReg" method="post" class="form-horizontal" action="">
                    <div class="form-group">
                        <label class="w-login__label" for="emailReg">Email</label>
                        <div class="input-field">
                            <input name="userid" type="email" class="form-control" id="emailReg" placeholder="Email" required>
                        </div>    
                    </div>                   
                    <div class="form-group">
                        <label class="w-login__label" for="passwordReg">Password</label>
                        <div class="input-field">
                            <input name="password" type="password" class="form-control" id="passwordReg" placeholder="Password" required>
                        </div>    
                    </div>                    
                    <button class="btn btn-default pull-right" id="quickstart-sign-up">Registrati</button>
                    </form>
                    <button class="btn btn-default pull-right" id="back-from-register">Indietro</button>
            </div>    
            <!--End Registrati-->                
                  
            
            <!-- Completa la registrazione-->
            <div id="complete-register">
                <h1 class="w-login__title">Completa la registrazione</h1>
                <form id="FormCompleteReg" method="post" class="form-horizontal" action="">  
                    <div class="form-group">
                        <label class="w-login__label" for="inputName">Nome</label>
                        <div class="input-field">
                            <input name="inputName" type="text" class="form-control" id="inputName" placeholder="Nome" required>
                        </div>                       
                    </div>
                    <div class="form-group">
                        <label class="w-login__label" for="inputSurname">Cognome</label>
                        <div class="input-field">
                            <input name="inputSurname" type="text" class="form-control" id="inputSurname" placeholder="Cognome" required>
                        </div>                          
                    </div>
                    <div class="form-group" id="passwordEmailAuth">
                        <label class="w-login__label" for="inputPassword">Password</label>
                        <div class="input-field">
                            <input name="inputPassword" type="text" class="form-control" id="inputPassword" placeholder="Password">
                        </div>                          
                    </div>                    
                    <div class="form-group">
                      <label class="w-login__label" for="inputTipology">Tipologia utente:</label>
                      <div class="input-field">
                        <select class="form-control inputUserType" id="inputUserType" required>
                          <option value="">--</option>
                          <option value="private">Privati</option>
                          <option value="professional">Professionisti</option>
                          <option value="agency">Agenzia</option>
                        </select>
                      </div>    
                    </div>            
                    <button class="btn btn-default pull-right" id="register-data">Invia</button>
                </form>

            </div>            
            
            <!-- End completa la registrazione -->
 
                <!-- User reset password -->
                <div id="user-reset-password" class="form-horizontal">
                    <h1 class="w-login__title">Reset password</h1>
                    <div id="firebaseui-container"></div>
                    <form id="FormReset" method="post" class="form-horizontal" action="">
                    <div class="form-group">
                        <label class="w-login__label" for="emailAddress">Email</label>
                        <div class="input-field">
                            <input name="emailAddress" type="email" class="form-control" id="emailAddress" placeholder="Email" required>
                        </div>  
                        <span><small>Inserisci il tuo indirizzo email</small></span>
                    </div>                                     
                    <button class="btn btn-default pull-right" id="reset-password">Reset</button>
                    </form>
                    <button class="btn btn-default pull-right" id="back-from-reset">Indietro</button>
                </div>  
                <!-- User reset password -->            
            <br /><br />
            <div class="alert alert-warning alert-dismissible fade in" id="alert">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Warning!</strong> <p id="alertMessage"></p>
            </div>            
            <p class="signup-block" id="signup-block">Non hai un account?
                <a href="#" class="custom-link custom-link--bolded" id="sign-up">Iscriviti</a>
            </p>            
            </div>  
        </section>      

	<script>
            window.onload = function() {
              initFirebase();
              initApp();
            };    
            
            var currentpage = 'login';
            
            $(document).ready(function () {
    
                 $('form').each(function () {  
                     //alert($(this).attr('id'));
                    $( this ).validate( { 
                        submitHandler: function() {
                            //alert("submit");
                            // your ajax would go here
                            return false;  // blocks regular submit since you have ajax
                        },                            
                        rules: {
                                password: {
                                        required: true,
                                        minlength: 4
                                },
                                emailAuth: {
                                        required: true,
                                        email: true,
                                        minlength: 4
                                },
                                passwordReg: {
                                        required: true,
                                        minlength: 4
                                },
                                emailReg: {
                                        required: true,
                                        email: true,
                                        minlength: 4
                                },                                
                                emailAddress: {
                                        required: true,
                                        email: true,
                                        minlength: 4
                                },
                                inputPassword: {
                                        required: true,
                                        minlength: 4
                                }
                        },
                        messages: {
                                password: {
                                        required: "Please provide a password",
                                        minlength: "Your password must be at least 4 characters long"
                                },
                                emailAuth: "Please enter a valid email address",                            
                                passwordReg: {
                                        required: "Please provide a password",
                                        minlength: "Your password must be at least 4 characters long"
                                },
                                inputPassword: {
                                        required: "Please provide a password",
                                        minlength: "Your password must be at least 4 characters long"
                                },                                
                                emailReg: "Please enter a valid email address",
                                emailAddress: "Please enter a valid email address"                              
                        },
                        errorElement: "em",
                        errorPlacement: function ( error, element ) {
                                // Add the `help-block` class to the error element
                                error.addClass( "help-block" );

                                // Add `has-feedback` class to the parent div.form-group
                                // in order to add icons to inputs
                                element.parents( ".input-field" ).addClass( "has-feedback" );

                                if ( element.prop( "type" ) === "checkbox" ) {
                                        error.insertAfter( element.parent( "label" ) );
                                } else {
                                        error.insertAfter( element );
                                }

                                // Add the span element, if doesn't exists, and apply the icon classes to it.
                                if ( !element.next( "span" )[ 0 ] ) {
                                        $( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
                                }
                        },
                        success: function ( label, element ) {
                                // Add the span element, if doesn't exists, and apply the icon classes to it.
                                if ( !$( element ).next( "span" )[ 0 ] ) {
                                        $( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
                                }
                        },
                        highlight: function ( element, errorClass, validClass ) {
                                $( element ).parents( ".input-field" ).addClass( "has-error" ).removeClass( "has-success" );
                                $( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
                        },
                        unhighlight: function ( element, errorClass, validClass ) {
                                $( element ).parents( ".input-field" ).addClass( "has-success" ).removeClass( "has-error" );
                                $( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
                        }

                    });
                    });
            });    
            
	</script>
  </body>
</html>
