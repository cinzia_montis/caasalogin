/**
 * 
 * @param {type} ui
 * @return {undefined}
 * Visualizza la gui per un utente non autenticato
 */
function displaySignedOutUser(ui) {
    console.log("displaySignedOutUser");     
    $("#user-signed-in").hide();
    $("#user-signed-out").show();
    $("#user-register").hide();
    $("#signup-block").show();
    $("#complete-register").hide();
    $("#user-reset-password").hide();
    $("#user-add-password").hide();
    $("#alert").hide();
    $("#quickstart-sign-in").prop('disabled', false);
    $("#quickstart-sign-in").click(function() {
        signIn();
    });
    $("#sign-up").click(function() {
           displaySignUp();
    });
    $("#show-reset-password").click(function() {
            displayResetPassword();
    });   
    ui.start('#firebaseui-container', getUiConfig());
};

/**
 * 
 * @param {type} user
 * @return {undefined}
 * Visualizza la gui per un utente registrato ma che deve completare la profilazione
 */
function displayUserProfile(user, inputPassword){
    console.log("displayUserProfile");
    if(inputPassword){  
        $("#passwordEmailAuth").show();
    }else{
        $("#passwordEmailAuth").hide();
    }     
    $("#user-signed-in").hide();
    $("#user-signed-out").hide();
    $("#user-register").hide();
    $("#signup-block").hide();
    $("#complete-register").show();
    $("#user-reset-password").hide();  
    $("#user-add-password").hide();
    $("#alert").hide();
    $("#register-data").click(function() {
        if (!$("#FormCompleteReg").valid()) {
            return;
        }else{        
            saveUserProfileData(user);
        }
        //alert(user);
    });
};

/**
 * Displays the UI for a signed in user.
 * @param {!firebase.User} user
 */
function displaySignedInUser(user) {
    console.log("displaySignedInUser");
    if(user.displayName !== null){
        console.log('displaySignedInUser: Benvenuto '+user.displayName);
        $("#user-signed-in").show();
        $("#user-signed-out").hide();
        $("#sign-out").show();
        $("#delete-account").show();
        $("#user-register").hide();
        $("#signup-block").hide();
        $("#complete-register").hide();
        $("#user-reset-password").hide();
        $("#user-add-password").hide();
        $("#alert").hide();
        $("#name").text("Benvenuto "+user.displayName);
    }/*else{
        displayUserProfile(user);
    }*/
 } 
 
function displayBackFromregister(){
    console.log('displayBackFromregister');
   $("#user-signed-in").hide();
   $("#user-signed-out").show();
   $("#user-register").hide();
   $("#signup-block").show();
   $("#complete-register").hide();
   $("#user-reset-password").hide();
   $("#user-add-password").hide();
   $("#alert").hide();
}

function displaySignUp(){
    console.log("displaySignUp");
    $("#user-signed-in").hide();
    $("#user-signed-out").hide();
    $("#user-register").show();
    $("#signup-block").hide();
    $("#complete-register").hide();
    $("#user-reset-password").hide();
    $("#user-add-password").hide();
    $("#alert").hide();
    $("#quickstart-sign-up").prop('disabled', false);
    $("#quickstart-sign-up").click(function() {
        signUp();
    });
    $("#back-from-register").click(function() {
            displayBackFromregister();
    });
}

function displayResetPassword(){
    console.log("displayResetPassword");
    $("#user-signed-in").hide();
    $("#user-signed-out").hide();
    $("#user-register").hide();
    $("#signup-block").show();
    $("#complete-register").hide();
    $("#user-reset-password").show();
    $("#user-add-password").hide();
    $("#alert").hide();
    $("#reset-password").click(function() {
        resetPassword();
    });
    $("#back-from-reset").click(function() {
            displayBackFromregister();
    });
}