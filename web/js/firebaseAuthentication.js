var ui;
var firebase;
var firebaseui;
var credential;
var googleCredential;
var isNewUser = false;


/**
 * Initializes the app.
 */
var ui;
function initApp() {
    console.log('initApp');
    // Initialize the FirebaseUI Widget using Firebase.
    ui = new firebaseui.auth.AuthUI(firebase.auth());               
    
    // Listen to change in auth state so it displays the correct UI for when
    // the user is signed in or not.
    firebase.auth().onAuthStateChanged(function(user) {     
        firebase.auth().languageCode = 'it';

        user ? signedInUser(user) : displaySignedOutUser(ui);    
    });    
       
    // Disable auto-sign in.
    ui.disableAutoSignIn();    
};

/**
 * Deletes the user's account.
 */
function deleteAccount() {
    console.log('deleteAccount');
  firebase.auth().currentUser.delete().catch(function(error) {
    if (error.code === 'auth/requires-recent-login') {
      // The user's credential is too old. She needs to sign in again.
      firebase.auth().signOut().then(function() {
          credential = "";
          googleCredential = "";
        // The timeout allows the message to be displayed after the UI has
        // changed to the signed out state.
        /*setTimeout(function() {
          alert('Please sign in again to delete your account.');
        }, 1);*/
      });
    }
  });
};


/**
 * Complete the user profilation.
 */
function saveUserProfileData(user){
        console.log('saveUserProfileData'); 
       if (user) {
        var userId = firebase.auth().currentUser.uid;
        var userType = $("#inputUserType").val();
        //var inputsValues = findInputs("form-"+userType, "form-control");     

        var name = $("#inputName").val();
        var surname = $("#inputSurname").val();
        var userType = $("#inputUserType").val();

        console.log(name+" - "+surname+" - "+userType)
        if (typeof(Storage) !== "undefined") {
            console.log("[ready] localStorage OK");
                
                firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
                  if (snapshot.val() && snapshot.val().caasaID && snapshot.val().visits && snapshot.val().lastVisit) {
                      console.log(snapshot.val().caasaID +" - "+caasaID);
                      if ( snapshot.val().caasaID !== caasaID ) {
                        console.log("[ready] caasaID in firebase != cookie value");
                        caasaID = snapshot.val().caasaID;
                        Cookies.set("caasaid", snapshot.val().caasaID, {domain: "caasa.it", expires: 365});
                      }
                      var visits = Number(snapshot.val().visits);
                      var lastVisit = Number(snapshot.val().lastVisit);                      
                                   
                        firebase.database().ref('/users/' + userId+'/caasaID').set(caasaID);
                        firebase.database().ref('/users/' + userId+'/visits').set(visits);
                        firebase.database().ref('/users/' + userId+'/lastVisit').set(lastVisit);                      
                        firebase.database().ref('/users/' + userId+'/userProfile/name').set(name);
                        firebase.database().ref('/users/' + userId+'/userProfile/surname').set(surname);
                        firebase.database().ref('/users/' + userId+'/userProfile/userType').set(userType);
                        firebase.database().ref('/users/' + userId+'/userProfile/email').set(user.email);

                        /*for (var index in inputsValues) {                            
                            firebase.database().ref('/users/' + userId+'/userProfile/'+index).set(inputsValues[index]);
                        }*/                                                                   
                  }
                        if($("#inputPassword").val()){
                            var newPassword = $("#inputPassword").val();
                                          
                            user.updatePassword(newPassword).then(function() {
                              console.log("password aggiornata");
                            }).catch(function(error) {
                              console.log("errore nell'update della password");
                            });  
                        }
                        user.updateProfile({
                          displayName: name+" "+surname
                        }).then(function() {
                            console.log("go to signedinuser");
                            //signedInUser(user);
                            if (user.emailVerified) {
                              console.log('Email is verified');
                            }else {
                              sendEmailVerification(user);  
                            }                             
                            displaySignedInUser(user);
                        }).catch(function(error) {
                          // An error happened.
                        });                                                    
                                      
                });      
                
            }
        }  
};


/**
 * Sign in user.
 * @param {!firebase.User} user
 */
function signedInUser(user) {  
    console.log('signedInUser');     
    displaySignedInUser(user);
    $("#email").text(user.email);

    $("#sign-out").click(function() {
      firebase.auth().signOut();
      credential = "";
      googleCredential = "";
    });  

    $("#delete-account").click(function() {
          deleteAccount();
    });  

    if (user.photoURL){
      var photoURL = user.photoURL;
      // Append size to the photo URL for Google hosted images to avoid requesting
      // the image with its original resolution (using more bandwidth than needed)
      // when it is going to be presented in smaller size.
      if ((photoURL.indexOf('googleusercontent.com') !== -1) ||
          (photoURL.indexOf('ggpht.com') !== -1)) {
        photoURL = photoURL + '?sz=100'; 
            //+ document.getElementById('photo').clientHeight;
      }
      $("#photo").attr('src', photoURL);
      $("#photo").show();
    } else {
      $("#photo").hide();
    }

      if (user) {
        var userId = firebase.auth().currentUser.uid;
        var searches = null;
        console.log("[ready.onAuthStateChanged] an "+(user.isAnonymous ? "anonymous " : "")+" user ("+userId+") logged-in");
        if (typeof(Storage) !== "undefined") {
            console.log("[ready] localStorage OK");
            setLastPageView();
            setPageViews();
            console.log("set lastPageView: "+getLastPageView() +" set pageviews: "+getPageViews());   
                firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
                  if (snapshot.val() && snapshot.val().caasaID && snapshot.val().visits && snapshot.val().lastVisit) {
                      console.log(snapshot.val().caasaID +" - "+caasaID);
                      if ( snapshot.val().caasaID !== caasaID ) {
                        console.log("[ready] caasaID in firebase != cookie value");
                        caasaID = snapshot.val().caasaID;
                        Cookies.set("caasaid", snapshot.val().caasaID, {domain: "caasa.it", expires: 365});
                      }
                      var visits = Number(snapshot.val().visits)+1;
                      console.log("[ready] updating user "+userId+" with "+visits+" visits");
                      firebase.database().ref('/users/' + userId+'/visits').set(visits);
                      firebase.database().ref('/users/' + userId+'/lastVisit').set(new Date().getTime());
                      if ( snapshot.val().searches )
                          searches = snapshot.val().searches;
                      else
                          searches = [];
                  } else {
                    console.log("[ready] creating new user "+userId);
                    firebase.database().ref('/users/' + userId).set({
                        caasaID: caasaID,
                        visits: 1,
                        lastVisit : new Date().getTime()
                    });
                
                    console.log("userProfile non ancora completato");
                    var inputPassword = false;
                    firebase.auth().fetchSignInMethodsForEmail(user.email).then(function(methods) {
                      console.log(methods.length +" - "+ methods[0]);
                         if(methods.length === 1 && methods[0] === ("google.com")){    
                             inputPassword = true;
                             displayUserProfile(user, inputPassword);
                          }else{
                              displayUserProfile(user, false);
                          }
                      });                   
                  }                  
                });
            
        }       
      } else {
         console.log("[ready.onAuthStateChanged] an user logged-out");
      } 
      
};

/**
 * Send an email verification
 * @return {undefined}
 */
function sendEmailVerification(user) {
    console.log('sendEmailVerification!');
    user.sendEmailVerification().then(function() {

        message = "Abbiamo inviato un'email a "+user.email+" per la verifica dell'indirizzo.";

        $("#alertMessage").text(message);
        $("#alert").show();
        setTimeout(function(){ $("#alert").hide(); }, 5000);
        console.log('Email Verification Sent!');
    });
};

/**
 * Sign up button.
 */
function signUp() {   
    console.log("signUp");
  var email = $("#emailReg").val();
  var password = $("#passwordReg").val();

  credential = firebase.auth.EmailAuthProvider.credential(email, password);
  firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    // [START_EXCLUDE]
    if (errorCode === 'auth/weak-password') {
        errorMessage = "La password e' troppo debole.";
    }   
        $("#alertMessage").text(errorMessage);
        $("#alert").show();          
      //alert('The password is too weak.');
 
    console.log("sign up: "+error);
  });
}
    
    
function signIn() {
    console.log('SignIn');
  if (firebase.auth().currentUser) {
    $("#sign-up").click(function() {
      firebase.auth().signOut();
      credential = "";
      googleCredential = "";
    });           
  } else {
    var email = $("#emailAuth").val();
    var password = $("#password").val();
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {

      var errorCode = error.code;
      var errorMessage = error.message;
      if(errorCode === "auth/user-not-found"){
          errorMessage = "Non esiste un utente corrispondente a questo identificativo. L'utente potrebbe essere stato cancellato.";
      }
      if(errorCode === "auth/wrong-password"){
          errorMessage = "La password inserita non è valida.";
      }
          $("#alertMessage").text(errorMessage);
          $("#alert").show();      
      //alert(errorCode +" - "+ errorMessage);
      console.log(errorCode +" - "+ errorMessage);
  
    });  
 
    }
  
   //$("#quickstart-sign-in").prop('disabled', true);
}    
    
    
function getUiConfig() {
    console.log('getUiConfig');     
  return {
    'callbacks': {
        'signInFailure': function(error) {
            //alert('signInFailure');
          errorMessage = "Log in fallito.";
      
          $("#alertMessage").text(errorMessage);
          $("#alert").show();             
        },   
      // Called when the user has been successfully signed in.
      'signInSuccessWithAuthResult': function(authResult, redirectUrl) {
        if (authResult.user) {
            googleCredential = firebase.auth.GoogleAuthProvider.credential(authResult.user.getAuthResponse().id_token); 
            console.log('signInSuccessWithAuthResult');
            signedInUser(user);                
        }
        // Do not redirect.
        return false;
      }
    },
    'autoUpgradeAnonymousUsers': true,
    // Opens IDP Providers sign-in flow in a popup.
    'signInFlow': 'popup',
    'signInOptions': [
      // TODO(developer): Remove the providers you don't need for your app.
      {
        provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        // Required to enable this provider in One-Tap Sign-up.
        authMethod: 'https://accounts.google.com',
        // Required to enable ID token credentials for this provider.
        clientId: CLIENT_ID,
      },
      {
        provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        scopes :[
          'public_profile',
          'email',
          'user_likes',
          'user_friends'
        ]
      }
    ],
    // Terms of service url.
    'tosUrl': 'https://www.google.com',
    'credentialHelper': CLIENT_ID && CLIENT_ID !== '578477636680-us48hmmnoare4fds98crd2js3ijfbnpl.apps.googleusercontent.com' ?
        firebaseui.auth.CredentialHelper.GOOGLE_YOLO :
        firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
  };
} 


function resetPassword(){
    console.log('resetPassword');
    var emailAddress = $("#emailAddress").val();
    firebase.auth().sendPasswordResetEmail(emailAddress).then(function() {
      //alert("Riceverai una mail al tuo indirizzo "+emailAddress);
        setTimeout(function() {
           message = "Riceverai una mail al tuo indirizzo "+emailAddress+" per il reset della password.";
            $("#alertMessage").text(message);
            $("#alert").show();         
        }, 300);          
      displaySignedOutUser(ui);
    }).catch(function(error) {
        error = "Non esiste un utente corrispondente a questo identificativo. L'utente potrebbe essere stato cancellato.";
        $("#alertMessage").text(error);
        $("#alert").show();         
         //alert('"'+emailAddress+'" - '+error);
         console.log('"'+emailAddress+'" - '+error);
    });
}
