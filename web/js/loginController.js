var caasaID = "caasaLogin";

/**
 * 
 * @param {element id} divId : il parent id
 * @param {element class} inputclass : la classe css
 * @return tutti i valori inseriti nel form 
 */
function findInputs(divId, inputclass) {
    var inputsValues = {};
    inputs = document.querySelectorAll('#'+divId+ ' .'+inputclass);
    for(var i=0; i<inputs.length; i++) {
        inputsValues[inputs[i].id] = document.getElementById(inputs[i].id).value;           
    };
            console.log(inputsValues);
    return inputsValues;
};

function setLastPageView(){
    sessionStorage.setItem("lastpageview", new Date().getTime());       
}

function getLastPageView(){
    return sessionStorage.lastpageview;;
}

function setPageViews(){
    if ( sessionStorage.pageviews ){
        sessionStorage.setItem("pageviews", Number(sessionStorage.pageviews) + 1);
    }else {
        sessionStorage.setItem("pageviews", 1);
    } 
}

function getPageViews(){
    return sessionStorage.pageviews;;
}
