var firebaseApp;
// 
// Google OAuth Client ID, needed to support One-tap sign-up. (https://developers.google.com/identity/one-tap/web/overview)
// Set to null if One-tap sign-up is not supported.
var CLIENT_ID = '578477636680-us48hmmnoare4fds98crd2js3ijfbnpl.apps.googleusercontent.com';
    
function initFirebase() {
    var firebaseConfig = {       
        apiKey: "AIzaSyAUFUaZLRnNLNkjqKXDv3yteTpgAUBLcXE",
        authDomain: "caasa-147511.firebaseapp.com",
        databaseURL: "https://caasa.firebaseio.com",
        projectId: "caasa-147511",
        storageBucket: "caasa-147511.appspot.com",
        messagingSenderId: "578477636680"      
    };

    firebaseApp = firebase.initializeApp(firebaseConfig);
    if(firebaseApp !== ""){
        console.log("[ready] firebase initialized OK");
        console.log(firebaseApp.name);
    }else{
         console.log("[ready] firebase initialized KO");
    }
   if ( firebaseApp.options.credential === firebaseConfig.credential
            && firebaseApp.options.databaseURL === firebaseConfig.databaseURL)
        console.log("[ready] firebase options OK");    
}
