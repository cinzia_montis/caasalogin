firebaseAuthentication.js: Descrizione delle funzioni


- initApp()
Al cambiare dello stato dell'utente mostra una GUI appropriata. Se l'utente è autenticato richiama signedInUser(user), se l'utente non è autenticato richiama displaySignedOutUser(ui).

- deleteAccount()
Elimina l'account di un utente autenticato

- saveUserProfileData(user)
Aggiunge dati di profilazione di un utente appena registrato. Al termine della procedura richiama signedInUser(user)

- signedInUser(user)
Mostra la GUI per un utente autenticato. Se l'utente non è nuovo aggiorna i dati di ultima visita e numero di visite nel DB. 
Se l'utente è un nuovo registrato inserisce i dati nel DB (lastVisit, visits, caasaID) e chiede di completare la profilazione dell'utente richiamando poi saveUserProfileData(user)

- sendEmailVerification(user)
Invia una mail per la verifica dell'indirizzo email

- signUp()
Crea un nuovo utente con la funzione firebase createUserWithEmailAndPassword(email, password)

- signIn()
Autentica l'utente con la funzione firebase signInWithEmailAndPassword(email, password)

- getUiConfig()
Crea la GUI con i pulsanti appartenenti ai providers abilitati nella console firebase. Premendo i pulsanti:
.se è un nuovo utente questo viene registrato e il suo account dovrà essere corredato di una password per il login con email. 
.se l'utente è già esistente eseguirà solamente il login

-resetPassword()
Invia una mail per il reset della password


Nella GUI credo sia necessario modificare l'aspetto grafico degli alert () e personalizzare i template delle email (reset password e verifica indirizzo email)

